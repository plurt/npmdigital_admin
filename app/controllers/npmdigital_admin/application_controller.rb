module NpmdigitalAdmin
  class ApplicationController < ActionController::Base
    before_action :authenticate_user!
    layout "npmdigital_admin/application_#{NpmdigitalAdmin.config.template_type}" # Template type initialized in engine or app

  end
end
