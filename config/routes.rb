NpmdigitalAdmin::Engine.routes.draw do
  devise_for :users, class_name: "NpmdigitalAdmin::User", :module => :devise
  root to: 'tests#index'

  namespace :examples do
    get 'basic', to: 'basic#index'
    get 'bbgrid', to: 'bbgrid#index'
    get 'dashboard', to: 'dashboard#index'
    get 'kitchensink', to: 'kitchensink#index'
    get 'topnav', to: 'topnav#index'
    get 'sidenav', to: 'sidenav#index'
  end
end