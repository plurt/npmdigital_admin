require 'rails/generators'
require File.expand_path('../utils', __FILE__)

module NpmdigitalAdmin
  class InstallGenerator < Rails::Generators::Base
    source_root File.expand_path('../templates', __FILE__)

    desc 'Npmadmin installation generator'

    def install
      route("mount NpmdigitalAdmin::Engine, at: 'admin'")
      template 'initializer.erb', 'config/initializers/npmdigital_admin.rb'
    end
  end
end