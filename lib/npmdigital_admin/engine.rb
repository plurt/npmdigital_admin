# Load foundation into the path early
require 'foundation-rails'
require 'foundation-icons-sass-rails'
require 'jquery-rails'
require 'jquery-datatables-rails'

module NpmdigitalAdmin
  class Engine < ::Rails::Engine
    isolate_namespace NpmdigitalAdmin

    config.to_prepare do
      Devise::DeviseController.layout "npmdigital_admin/application_authentication"
    end

  end

  def self.setup(&block)
      @@config ||= NpmdigitalAdmin::Engine::Configuration.new

      yield @@config if block

      return @@config
  end

  def self.config
      Rails.application.config
  end

end
