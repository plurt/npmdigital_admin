$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "npmdigital_admin/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "npmdigital_admin"
  s.version     = NpmdigitalAdmin::VERSION
  s.authors     = ["Clay Powers"]
  s.email       = ["clay.powers@northpoint.org"]
  s.homepage    = "http://npmdigital.org"
  s.summary     = "Admin system for NPM Digital Rails Applications."
  s.description = "Brings in a consistent theme and configurable login system. For use in any of NPM Digitals Rails applications."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "foundation-rails" #TODO - Set version
  s.add_dependency "foundation-icons-sass-rails"
  s.add_dependency "jquery-rails"
  s.add_dependency "rails", "~> 4.2.3"
  s.add_dependency "sass-rails"
  s.add_dependency "jquery-datatables-rails", "~> 3.3.0"
  s.add_dependency "devise"
  s.add_development_dependency "sqlite3"
end
